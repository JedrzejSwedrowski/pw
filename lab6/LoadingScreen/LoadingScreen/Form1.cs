﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace BackgroundWorkerSample
{
    public partial class Form1 : Form
    {
        BackgroundWorker m_oWorker;

        public Form1()
        {
            InitializeComponent();
            m_oWorker = new BackgroundWorker();
            m_oWorker.DoWork += new DoWorkEventHandler(m_oWorker_DoWork);
            m_oWorker.ProgressChanged += new ProgressChangedEventHandler(m_oWorker_ProgressChanged);
            m_oWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(m_oWorker_RunWorkerCompleted);
            m_oWorker.WorkerReportsProgress = true;
            m_oWorker.WorkerSupportsCancellation = true;
        }

        int pp = 0;
        int kp = 0;

        bool czy_pierwsza(int n)
        {
            if (n < 2)
                return false;

            for (int i = 2; i * i <= n; i++)
                if (n % i == 0)
                    return false;
            return true;
        }

        void m_oWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                label1.Text = "Zadanie przerwane";
                label2.Text = " ";
            }
            else if (e.Error != null)
            {
                label1.Text = "Błąd w trakcie wykonywania zadania";
            }
            else
            {
                label1.Text = "Zadanie wykonane";
            }
            button1.Enabled = true;
            button2.Enabled = false;
        }

        void m_oWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            label1.Text = "Przetwarzam... ";
            label2.Text = progressBar1.Value.ToString() + "%";
        }

        void m_oWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Random rnd = new Random();
            pp = 1;
            kp = 1000;
            int kplos = kp + 1;
            for (int i = 0; i < 100; i++)
            {
                int n = rnd.Next(pp, kplos);
                Thread.Sleep(100);
                if (czy_pierwsza(n))
                {
                    m_oWorker.ReportProgress(i);
                    Debug.WriteLine(n);
                }

                if (m_oWorker.CancellationPending)
                {
                    e.Cancel = true;
                    m_oWorker.ReportProgress(0);
                    return;
                }

            }
            m_oWorker.ReportProgress(100);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.Enabled = true;
            m_oWorker.RunWorkerAsync();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (m_oWorker.IsBusy)
            {
                m_oWorker.CancelAsync();
                label2.Text = " ";
            }
        }
    }
}
