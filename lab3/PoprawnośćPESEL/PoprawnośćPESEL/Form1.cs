﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PoprawnośćPESEL
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int[] value = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };
            int wynik = 0;
            int i = 0;
            int j = 0;
            string PESEL = textBox1.Text;
            string oblicz;
            int pozytyw = 10;

            if (PESEL == "")
            {
                label4.Text = "Brak danych";
                label4.ForeColor = Color.Red;
                return;
            }

            for (j = 0; j < 10; j++)
            {
                wynik += int.Parse(PESEL[j].ToString()) * value[i];
                i++;
            }

            oblicz = wynik.ToString();

            oblicz = oblicz[oblicz.Length - 1].ToString();

            pozytyw -= int.Parse(oblicz);

            if (pozytyw.ToString() == PESEL[10].ToString())
            {
                label4.Text = "Prawidlowy";
                label4.ForeColor = Color.Lime;
            }
            else
            {
                label4.Text = "Błędny";
                label4.ForeColor = Color.Red;
            }
        }
    }

}
