﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BMR
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text != "") && (textBox2.Text != "") && (textBox3.Text != ""))
            {
                label7.ForeColor = Color.Black;
                label9.ForeColor = Color.Black;
                int masaciala = Convert.ToInt32(textBox3.Text);
                float wzrost = Convert.ToSingle(textBox2.Text);
                int wiek = Convert.ToInt32(textBox1.Text);
                double mbmr = (9.99 * masaciala) + (6.25 * wzrost) - (4.92 * wiek) + 5;
                double fbmr = (9.99 * masaciala) + (6.25 * wzrost) - (4.92 * wiek) - 161;

                label7.Text = mbmr.ToString();
                label9.Text = fbmr.ToString();
            }
            else
            {
                label7.ForeColor = Color.Red;
                label7.Text = "Brak danych";
                label9.ForeColor = Color.Red;
                label9.Text = "Brak danych";
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

            private void label7_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
