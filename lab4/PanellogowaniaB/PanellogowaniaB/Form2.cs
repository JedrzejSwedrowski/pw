﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PanellogowaniaB
{
    public partial class Form2 : Form {
        public string ID, haslo;
        string IDadmin = "admin";
        string hadmin = "admin";
        string IDuser = "user";
        string huser = "user";

        Hashcode hc = new Hashcode();

        private void Form2_KeyPress(object sender, KeyPressEventArgs e) {
            if (e.KeyChar == 'q') Close();
        }

        private void Form2_Load(object sender, EventArgs e){
            string hcadmin = hc.PassHash(hadmin);
                string hcuser = hc.PassHash(huser);
            
            
            if (ID == IDadmin)
            {
                if (haslo == hcadmin)
                {
                    label1.ForeColor = Color.Lime;
                    label1.Text = "Zalogowałeś się poprawnie!";
                }
                else
                {
                    label1.ForeColor = Color.Red;
                    label1.Text = "Błędne ID lub hasło!";
                }
            }
            else
            {
                label1.ForeColor = Color.Red;
                label1.Text = "Błędne ID lub hasło!";
            }

            if (ID == IDuser)
            {
                if (haslo == hcuser)
                {
                    label1.ForeColor = Color.Lime;
                    label1.Text = "Zalogowałeś się poprawnie!";
                }
                else
                {
                    label1.ForeColor = Color.Red;
                    label1.Text = "Błędne ID lub hasło!";
                }
            }
        }

        public Form2()
        {
            InitializeComponent();
        }
    }
}
