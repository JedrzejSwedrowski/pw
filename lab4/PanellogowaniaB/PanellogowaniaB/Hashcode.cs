﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace PanellogowaniaB {
    class Hashcode {
        public string PassHash(string data)
        {
            MD5 MD5 = MD5.Create();
            byte[] hashdata = MD5.ComputeHash(Encoding.Default.GetBytes(data));
            StringBuilder returnValue = new StringBuilder();

            for (int i = 0; i < hashdata.Length; i++){
                returnValue.Append(hashdata[i].ToString());
            }
            return returnValue.ToString();
        }
    }
}
