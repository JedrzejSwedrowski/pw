﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kostka_do_gry
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Random rand = new Random();
            int[] sciany = new int[6];

            if (e.KeyChar == 'r')
            {
                for (int i = 0; i <= 5; i++)
                {
                    sciany[i] = rand.Next(0, 6)+1;
                    label2.Text = sciany[i].ToString();
                }
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(label2.Text);
        }
    }
}
